/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
  $("#btn-detail-result").click(function () {
    $(".full-background").addClass("active");
    $(".popup-result").addClass("active");
  });
  $(".btn-close").click(function () {
    $(".full-background").removeClass("active");
    $(".popup-result").removeClass("active");
    $(".popup-addaccount").removeClass("active");
    $(".popup-thongbao1").removeClass("active");
    $(".popup-thongbao2").removeClass("active");
    $(".popup-change-pass").removeClass("active");
    $(".popup-thongtin").removeClass("active");
    $(".thanhtoan").removeClass("active");
    $(".manage-login").removeClass("active");
    $(".popup-package").removeClass("active");
    $(".popup-kichhoat").removeClass("active");
  });
  $(".full-background").click(function () {
    $(".full-background").removeClass("active");
    $(".popup-result").removeClass("active");
    $(".popup-addaccount").removeClass("active");
    $(".popup-thongbao1").removeClass("active");
    $(".popup-thongbao2").removeClass("active");
    $(".popup-change-pass").removeClass("active");
    $(".popup-checkmail").removeClass("active");
    $(".popup-thongtin").removeClass("active");
    $(".thanhtoan__popup").removeClass("active");
    $(".manage-login").removeClass("active");
    $(".popup-package").removeClass("active");
    $(".popup-kichhoat").removeClass("active");
  });
  $(".btn-add-account").click(function () {
    $(".full-background").addClass("active");
    $(".popup-addaccount").addClass("active");
  });
  $(".btn-xephang").click(function () {
    $(".popup-xephang").toggleClass("active");
  });
});