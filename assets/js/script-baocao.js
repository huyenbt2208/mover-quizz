/*!
 * thuynt
 * 
 * 
 * @author thuynt
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
  $(function () {
    $('input[name="daterange"]').daterangepicker({
      opens: 'left'
    }, function (start, end, label) {
      console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    });
  });
  /*Super easy and 100% responsive chart using chartsjs plugin*/

  /*global console*/

  var ctx = document.getElementById('lineChart').getContext('2d');
  var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',
    // The data for our dataset
    data: {
      labels: ["1/2019", "2/2019", "3/2019", "4/2019", "5/2019", "6/2019", "7/2019", "8/2019", "9/2019", "10/2019"],
      datasets: [{
        label: "My First dataset ",
        fill: false,
        backgroundColor: 'rgb(255,114,20)',
        borderColor: 'rgb(255,114,20)',
        data: [0, 10, 20, 30, 40, 50, 60, 0, 0, 0, 0]
      }]
    },
    // Configuration options go here
    options: {
      legend: {
        onClick: null
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 0,
          bottom: 0
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            color: "rgba(0, 0, 0, 0)"
          }
        }],
        yAxes: [{
          gridLines: {
            color: "#e5e5e5"
          },
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
});